<?php

namespace WebComponents;

use WebComponents\Misc\AbstractDOM;

class Icons extends AbstractDOM {

	protected $element = 'i';
	protected $attributes = [];
	protected $text = null;


	/**
	* Set custom element tags
	* @return $this
	*/
	public function setElement($element) {
		$this->element = $element;

		return $this;
	}

	public function getElement() {
		return $this->element;
	}

	/**
	* Set element ettributes
	* @return $this
	*/
	public function setAttributes(array $attributes) {
		$this->attributes = $attributes;

		return $this;
	}

	public function getAttributes() {
		return $this->attributes;
	}

	/**
	* Set element text
	* @return $this
	*/
	public function setText($text) {
		$this->text = $text;

		return $this;
	}

	public function getText() {
		return $this->text;
	}

	/**
	* Render component as user configured
	* @return String HTML tags
	*/
	public function render() {
		$domDocument = $this->domDocument;
		$element = $domDocument->createElement($this->getElement());

		// If root element attributes setting is not empty, build its attributes 
		if(!empty($this->getAttributes())) {
			foreach ($this->getAttributes as $name => $attribute) {
				if(!$element->hasAttribute($name)) {
					$element->setAttribute($name, $attributes);
				}
			}
		}

		// Checking text availability
		if(!is_null($this->getText()) OR $this->getText() != '') {
			$text = $domDocument->createTextNode($this->getText());
			$element->appendChild($text);
		}

		$domDocument->appendChild($element);

		return $this->extractHTML($domDocument);
	}

}