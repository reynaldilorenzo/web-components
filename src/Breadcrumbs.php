<?php

namespace WebComponents;

use Exception;
use WebComponents\Icons;
use WebComponents\Traits\DOMDocumentTrait;

class Breadcrumbs {

	use DOMDocumentTrait;
	
	protected $parts = [];
	protected $iconElement = [
		'element' => 'i',
		'attributes' => []
	];
	protected $anchorElement = [
		'element' => 'a',
		'attributes' => []
	];
	protected $listRootElement = [
		'element' => 'ul',
		'attributes' => []
	];
	protected $listItemElement = [
		'element' => 'li',
		'attributes' => []
	];

	/**
	* Simple Setter-Getter helper using PHP Magic Method.
	* To call property of this class, just prefixed it with
	* set or get and uppercased first letter of property name, 
	* example :
	* 	setIconElement('foo')
	*		getIconElement()
	*/
	public function __call($name, $value) {
		$callingMethod = substr($name, 0, 3);
		$calledMethod = substr($name, 3);
		$originalPropertyName = lcfirst($calledMethod);
		$allowedPrefix = ['set', 'get'];

		if(!in_array($callingMethod, $allowedPrefix)) {
			throw new Exception("Unknown calling method '{$callingMethod}'");
		}

		if(!property_exists($this, $originalPropertyName)) {
			throw new Exception("Cannot find property '{$originalPropertyName}' in : ".__CLASS__);
		} 

		if($callingMethod == 'set') {
			$this->$originalPropertyName = $value;
		} else {
			return $this->$originalPropertyName;
		}

	}

	/**
	* @param Array $component, here list of available keys :
	*  - name : Display name to be showed
	*	 - url : anchor URL
	*	 - icon : icon name or identifier
	*/
	public function add(array $component) {
		if(isset($component['icon'])) {
			if(!is_array($component['icon']) OR !($component['icon'] instanceof Icons)) {
				throw new Exception("Icon mus be instance of ". Icons::class. " or array.");
			}
		}

		array_push($this->parts, $component);

		return $this;
	}

	public function render() {
		$domDocument = new DOMDocument('1.0');

		$root = $domDocument->createElement($this->listRootElement['element']);

		// If root element attributes setting is not empty, build its attributes 
		if(!empty($this->listRootElement['attributes'])) {
			foreach ($this->listRootElement['attributes'] as $attributeName => $attributeValue) {
				if(!$root->hasAttribute($attributeName)) {
					$root->setAttribute($attributeName, $attributeValue);
				}
			}
		}

		foreach ($this->parts as $key => $part) {
			$item = $domDocument->createElement($this->listItemElement['element']);
			$icon = null;

			// Check if icon option on part is exist, if true then build it
			if(isset($part['icon'])) {
				$defaultIconSetting = $this->iconElement;
				$icon = $domDocument->createElement($defaultIconSetting['element']);

				if(isset($part['icon']['class'])) {
					if(!isset($defaultIconSetting['attributes']['class'])) $defaultIconSetting['attributes']['class'] = [];
					
					$mergeClassAttribute = array_merge($defaultIconSetting['attributes']['class'], [$part['icon']['class']]);
					$defaultIconSetting['attributes']['class'] = implode(' ', $mergeClassAttribute);
				}

				// If icon attributes setting is not empty, build its attributes 
				if(!empty($defaultIconSetting['attributes'])) {
					foreach ($defaultIconSetting['attributes'] as $attributeName => $attributeValue) {
						if(!$icon->hasAttribute($attributeName)) {
							$icon->setAttribute($attributeName, $attributeValue);
						}
					}
				}

				if(isset($part['icon']['text'])) {
					$iconText = $domDocument->createTextNode($part['icon']['text']);
					$icon->appendChild($iconText);
				}
			}

			// Check if current part is last parts
			if($key == count($this->parts) - 1) {
				$activeText = $domDocument->createTextNode($part['name']);
				// Check if icon exist
				if(!is_null($icon)) $item->appendChild($icon);
				$item->appendChild($activeText);
			} else {
				$anchor = $domDocument->createElement($this->anchorElement['element']);
				// Checking if URL is given, if not assign automatically to root url
				$itemUrl = !isset($part['url']) ? '/' : $part['url'];
				$anchor->setAttribute('href', $itemUrl);

				// If anchor attributes setting is not empty, build its attributes 
				if(!empty($this->anchorElement['attributes'])) {
					foreach ($this->anchorElement['attributes'] as $attributeName => $attributeValue) {
						if(!$anchor->hasAttribute($attributeName)) {
							$anchor->setAttribute($attributeName, $attributeValue);
						}
					}
				}

				$anchorText = $domDocument->createTextNode($part['name']);
				// Check if icon exist
				if(!is_null($icon)) $anchor->appendChild($icon);
				$anchor->appendChild($anchorText);

				$item->appendChild($anchor); 
			}

			$root->appendChild($item);
		}

		$domDocument->appendChild($root);

		return $this->extractHTML($domDocument);
	}

}