<?php

namespace WebComponents\Miscs;

use DOMDocument;
use WebComponents\Icons;

abstract class AbstractDOM {
	
	protected $domDocument;


	public function __construct() {
		$this->domDocument = new DOMDocument;
	}

	abstract public function render();

	/**
	* Check if icon key on attribute is exist
	* @return boolean
	*/
	public function isAttributeIconExist(array $attributes) {
		return array_key_exists('icon', $attributes);
	}

	/**
	* As of version 5.2.6, saveHTML() will automatically
	* add basic HTML structure. As suggested in :
	* @link // http://sg2.php.net/manual/en/domdocument.savehtml.php#85165
	* i should remove these structure to only get HTML element needed
	*/
	public function extractHTML(DOMDocument $document) {
		return preg_replace('/^<!DOCTYPE.+?>/', '', 
						str_replace( 
							array('<html>', '</html>', '<body>', '</body>'), 
							array('', '', '', ''),
							 $document->saveHTML()
						)
					);
	}

}